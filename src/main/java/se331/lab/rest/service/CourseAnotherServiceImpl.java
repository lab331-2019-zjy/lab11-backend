package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseDao courseDao;

    List<Course> allCourses;
    @Override
    public ArrayList<Course> getCourseWhichStudentsEnrolledMoreThan(int x) {
        allCourses = courseDao.getAllCourses();
        ArrayList<Course> result = new ArrayList<>();
        for (Course c : allCourses){
            if(c.getStudents().size() > x){
                result.add(c);
            }
        }
        return result;
    }
}
