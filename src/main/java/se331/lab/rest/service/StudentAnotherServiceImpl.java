package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class StudentAnotherServiceImpl implements StudentAnotherService {
    @Autowired
    LecturerAnotherDao lecturerAnotherDao;
    @Autowired
    StudentDao studentDao;

    List<Student> allStudents;


    @Override
    public ArrayList<Student> getStudentByNameContains(String partOfName) {
        allStudents = this.studentDao.getAllStudent();
        ArrayList<Student> result = new ArrayList<>();
        for (Student s : allStudents) {
            if (s.getName().contains(partOfName) || s.getSurname().contains(partOfName)) {
                result.add(s);
            }
        }
        return result;
    }

    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String name) {
        List<Student> result;
        result = lecturerAnotherDao.getLecturerByLastName(name).getAdvisees();
        return result;
    }
}
