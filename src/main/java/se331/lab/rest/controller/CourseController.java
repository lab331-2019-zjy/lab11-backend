package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;
import se331.lab.rest.service.CourseService;
import se331.lab.rest.service.LecturerService;

@Controller
public class CourseController {
    @Autowired
    CourseService courseService;
    @Autowired
    CourseAnotherService courseAnotherService;

    @GetMapping("/courses")
    public ResponseEntity getAllLecturer() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseService.getAllCourses()));
    }

    @GetMapping("/getCourseWhichStudentEnrolledMoreThan/{x}")
    public ResponseEntity getCourseWhichStudentEnrolledMoreThan(@PathVariable("x") int x) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseAnotherService.getCourseWhichStudentsEnrolledMoreThan(x)));
    }
}
